/**
 * @file    examples.c
 * @author  Paul Thomas
 * @date    5/28/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "strato_test_interface.h"

#pragma clang diagnostic push
#pragma ide diagnostic   ignored "EndlessLoop"
#pragma ide diagnostic   ignored "ConstantConditionsOC"
#pragma ide diagnostic   ignored "UnreachableCode"
#define REQUEST 1
#define REPLY   0

#undef UNUSED
#define UNUSED(x) (void)x

#define BUFFER_LENGTH 256
static unsigned char recv_buffer[BUFFER_LENGTH];

/* Override weak function */
void strato_test_interface_request_on_set_ao_value_payload(const strato_test_interface_set_ao_mode_payload_t* payload)
{
  /* Do something with the payload */

  /* reply to user */
  unsigned short                            reply_length = BUFFER_LENGTH;
  strato_test_interface_conversion_result_e result       = strato_test_interface_serialize_command(STC_SET_AO_VALUE, REPLY, payload, recv_buffer, &reply_length);
  if (result != STCR_SUCCESS) {
    /* handle error */
    return;
  }

  /* send reply */
}

void strato_test_interface_reply_on_set_ao_value_payload(const strato_test_interface_set_ao_mode_payload_t* payload)
{
  UNUSED(payload);
  /* Do something with payload */
  /* handle success */
}

/* Use can override the rest of the other request/reply functions */

unsigned char available(unsigned char* buffer, unsigned short* length)
{
  UNUSED(buffer);
  UNUSED(length);
  /* fetch data from interface */
  return 0;
}


int main()
{
  while (1) {
    unsigned short length = BUFFER_LENGTH;
    unsigned char  buffer[BUFFER_LENGTH];
    if (available(buffer, &length)) {
      /* If has data, send to dispatcher */
      strato_test_interface_dispatcher(buffer, length);
    }
  }

  return 0;
}

#pragma clang diagnostic pop
