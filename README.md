# Strato test interface

Utility library for interfacing Strato product with external test station.

It provides multiples types and commands to be used in test plan.

## Usage

See [example](.\examples\example.c)

Library provide commands types, serialization/deserialization functions and request/reply handlers automatically called
by a common dispatcher.

User is expected to override handlers.

