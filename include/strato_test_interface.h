#ifndef STRATO_TEST_INTERFACE_H
#define STRATO_TEST_INTERFACE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  STCR_UNKNOWN,
  STCR_SUCCESS,
  STCR_ERROR
} strato_test_interface_conversion_result_e;
typedef enum {
  STC_UNKNOWN,
  STC_PING, /* test serial port is working */
  STC_SET_RELAY,
  STC_SET_AI_MODE,  /* set analog input mode */
  STC_READ_AI,      /* read analog input voltage */
  STC_SET_AO_MODE,  /* set analog output mode */
  STC_SET_AO_VALUE, /* set analog output value */
  STC_READ_RADAR,
  STC_READ_CO2,
  STC_READ_TEMP,
  STC_READ_HUM,
  STC_SET_LCD_TEST,
  STC_READ_LUM,
  STC_READ_BUTTONS,
  STC_RECEIVE_RS485, /* Request device to receive on RS485, see documentation/rs485.md */
  STC_SEND_RS485,    /* Request device to send on RS485, see documentation/rs485.md */
  STC_GET_WIFI_SSID,
  STC_GET_WIFI_PASSKEY,
  STC_TEST_FLASH,
  STC_GET_RFID_CONTENT,
  STC_SET_RFID_CONTENT,
  STC_GET_VERSION,
  STC_GET_MAC,
  STC_ERROR,
  STC_MAX
} strato_test_interface_command_e;

typedef struct {
  unsigned char index; /* 0 means all */
  unsigned char state; /* 0 means open, 1 means closed */
} strato_test_interface_set_relay_payload_t;

typedef struct {
  unsigned char index; /* 0 means all */
  enum staim {
    STAIM_UNKNOWN,
    STAIM_V_IN,
    STAIM_THERM,
    STAIM_I_IN,
    STAIM_MAX
  } mode;
} strato_test_interface_set_ai_mode_payload_t;

typedef struct {
  unsigned char index; /* [1, 8]. 1 == AI1, 8 == AI8 */
} strato_test_interface_read_ai_value_request_payload_t;

typedef struct {
  unsigned char  index; /* [1, 8]. 1 == AI1, 8 == AI8 */
  unsigned short value;
} strato_test_interface_read_ai_value_reply_payload_t;

typedef struct {
  enum staom {
    STAOM_UNKNOWN,
    STAOM_AOUT,
    STAOM_MAX
  } mode;
} strato_test_interface_set_ao_mode_payload_t;

typedef struct {
  unsigned char index; /* [1, 3]. 1 == AO1, 3 == AO3 */
  unsigned char value; /* 100mV step */
} strato_test_interface_set_ao_value_payload_t;

typedef struct {
  unsigned char value; /* 0: No obstruction, 1: obstructed */
} strato_test_interface_read_radar_reply_payload_t;

typedef struct {
  unsigned short value; /* co2 ppm */
} strato_test_interface_read_co2_reply_payload_t;

typedef enum {
  STRTC_UNKNOWN,
  STRTC_TOP,
  STRTC_MIDDLE,
  STRTC_BOTTOM,
  STRTC_MAX
} strato_test_interface_read_temp_channel_e;

typedef struct {
  strato_test_interface_read_temp_channel_e channel;
} strato_test_interface_read_temp_request_payload_t;

typedef struct {
  strato_test_interface_read_temp_channel_e channel;
  float                                     value;
} strato_test_interface_read_temp_reply_payload_t;

typedef struct {
  unsigned short value;
} strato_test_interface_read_hum_reply_payload_t;

typedef struct {
  enum stslt {
    STSLT_UNKNOWN,
    STSLT_OFF,
    STSLT_TEST,
    STSLT_BLACK,
    STSLT_RED,
    STSLT_GREEN,
    STSLT_BLUE,
    STSLT_WHITE,
    STSLT_MAX
  } screen;
  unsigned char backlight_brightness_pct; /* Brightness percentage of the backlight, [0-100] */
  /* Note: When doing RGB test, backlight brightness must be at 100% */
} strato_test_interface_set_lcd_test_payload_t;

typedef struct {
  unsigned short value;
} strato_test_interface_read_lum_reply_payload_t;

typedef struct {
  unsigned char pressed[4]; /* 0 not pressed, 1 pressed */
} strato_test_interface_read_buttons_reply_payload_t;

typedef struct {
  unsigned char index;
  unsigned char length;
} strato_test_interface_receive_rs485_request_payload_t;

typedef struct {
  unsigned char index;
  enum strx {
    STRX_RS485_UNKNOWN,
    STRX_RS485_SUCCESS,
    STRX_RS485_ERROR
  } result;
} strato_test_interface_receive_rs485_reply_payload_t;

typedef struct {
  unsigned char     index;
  unsigned char     length;
  unsigned long int delay_ms; /* Duration to wait before sending payload on RS485 interface */
} strato_test_interface_send_rs485_request_payload_t;

typedef struct {
  unsigned char index;
  enum sttx {
    STTX_RS485_UNKNOWN,
    STTX_RS485_SUCCESS,
    STTX_RS485_ERROR_NO_DATA,
    STTX_RS485_ERROR_GENERIC
  } result;
} strato_test_interface_send_rs485_reply_payload_t;

typedef struct {
  unsigned short length;
  const char*    ssid;
} strato_test_interface_get_wifi_ssid_reply_payload_t;

typedef struct {
  unsigned short length;
  const char*    passkey;
} strato_test_interface_get_wifi_passkey_reply_payload_t;

typedef struct {
  unsigned char result; /* 0 : FAIL, 1 : OK */
} strato_test_interface_test_flash_reply_payload_t;

typedef struct {
  unsigned short       length;
  const unsigned char* content;
} strato_test_interface_get_rfid_content_reply_payload_t;

typedef struct {
  unsigned short       length;
  const unsigned char* content;
} strato_test_interface_set_rfid_content_payload_t;

typedef struct {
  char firmwareVersion[6];
  char bootloaderVersion[6];
  char hardwareVersion[4];
  char applicationVersion[6];
} strato_test_interface_get_version_reply_payload_t;

typedef struct {
  const unsigned char mac[6];
} strato_test_interface_get_mac_reply_payload_t;

typedef struct {
  strato_test_interface_command_e command;
} strato_test_interface_error_t;

strato_test_interface_conversion_result_e strato_test_interface_serialize_command(strato_test_interface_command_e command, unsigned char is_request, const void* payload, unsigned char* buffer, unsigned short* length);
strato_test_interface_conversion_result_e strato_test_interface_deserialize_command(const unsigned char* buffer, unsigned short length, strato_test_interface_command_e command, unsigned char is_request, void* payload);

void strato_test_interface_dispatcher(const unsigned char* buffer, unsigned short length);

void strato_test_interface_request_on_unknown(const unsigned char* buffer, unsigned short length);
void strato_test_interface_request_on_ping(void);
void strato_test_interface_request_on_set_relay(const strato_test_interface_set_relay_payload_t* request_payload);
void strato_test_interface_request_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t* request_payload);
void strato_test_interface_request_on_read_ai(const strato_test_interface_read_ai_value_request_payload_t* request_payload);
void strato_test_interface_request_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t* request_payload);
void strato_test_interface_request_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t* request_payload);
void strato_test_interface_request_on_read_radar(void);
void strato_test_interface_request_on_read_co2(void);
void strato_test_interface_request_on_read_temp(const strato_test_interface_read_temp_request_payload_t* request_payload);
void strato_test_interface_request_on_read_hum(void);
void strato_test_interface_request_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t* request_payload);
void strato_test_interface_request_on_read_lum(void);
void strato_test_interface_request_on_read_buttons(void);
void strato_test_interface_request_on_receive_rs485(const strato_test_interface_receive_rs485_request_payload_t* request_payload);
void strato_test_interface_request_on_send_rs485(const strato_test_interface_send_rs485_request_payload_t* request_payload);
void strato_test_interface_request_on_get_wifi_ssid(void);
void strato_test_interface_request_on_get_wifi_passkey(void);
void strato_test_interface_request_on_test_flash(void);
void strato_test_interface_request_on_get_rfid_content(void);
void strato_test_interface_request_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t* request_payload);
void strato_test_interface_request_on_get_version(void);
void strato_test_interface_request_on_get_mac(void);

void strato_test_interface_reply_on_unknown(const unsigned char* buffer, unsigned short length);
void strato_test_interface_reply_on_ping(void);
void strato_test_interface_reply_on_set_relay(const strato_test_interface_set_relay_payload_t* reply_payload);
void strato_test_interface_reply_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t* reply_payload);
void strato_test_interface_reply_on_read_ai(const strato_test_interface_read_ai_value_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t* reply_payload);
void strato_test_interface_reply_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t* reply_payload);
void strato_test_interface_reply_on_read_radar(const strato_test_interface_read_radar_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_read_co2(const strato_test_interface_read_co2_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_read_temp(const strato_test_interface_read_temp_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_read_hum(const strato_test_interface_read_hum_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t* reply_payload);
void strato_test_interface_reply_on_read_lum(const strato_test_interface_read_lum_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_read_buttons(const strato_test_interface_read_buttons_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_receive_rs485(const strato_test_interface_receive_rs485_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_send_rs485(const strato_test_interface_send_rs485_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_get_wifi_ssid(const strato_test_interface_get_wifi_ssid_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_get_wifi_passkey(const strato_test_interface_get_wifi_passkey_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_test_flash(const strato_test_interface_test_flash_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_get_rfid_content(const strato_test_interface_get_rfid_content_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t* reply_payload);
void strato_test_interface_reply_on_get_version(const strato_test_interface_get_version_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_get_mac(const strato_test_interface_get_mac_reply_payload_t* reply_payload);
void strato_test_interface_reply_on_error(const strato_test_interface_error_t* reply_payload);

#ifdef __cplusplus
}
#endif

#endif
