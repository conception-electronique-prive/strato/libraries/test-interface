/**
 * @file    tests.cpp
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include <gtest/gtest.h>

#include "strato_test_interface.h"
#undef UNUSED
#define UNUSED(x) (void)x

enum strato_test_interface_command_kind_e {
  STCK_UNKNOWN,
  STCK_REQUEST,
  STCK_REPLY,
} handled_command_kind = STCK_UNKNOWN;

strato_test_interface_command_e handled_command = STC_UNKNOWN;

void strato_test_interface_request_on_unknown(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  handled_command      = STC_UNKNOWN;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_ping()
{
  handled_command      = STC_PING;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_relay(const strato_test_interface_set_relay_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_RELAY;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_AI_MODE;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_ai(const strato_test_interface_read_ai_value_request_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_READ_AI;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_AO_MODE;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_AO_VALUE;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_radar()
{
  handled_command      = STC_READ_RADAR;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_co2()
{
  handled_command      = STC_READ_CO2;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_temp(const strato_test_interface_read_temp_request_payload_t *payload)
{
  UNUSED(payload);
  handled_command      = STC_READ_TEMP;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_hum()
{
  handled_command      = STC_READ_HUM;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_LCD_TEST;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_lum()
{
  handled_command      = STC_READ_LUM;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_read_buttons()
{
  handled_command      = STC_READ_BUTTONS;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_receive_rs485(const strato_test_interface_receive_rs485_request_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_RECEIVE_RS485;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_send_rs485(const strato_test_interface_send_rs485_request_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SEND_RS485;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_get_wifi_ssid()
{
  handled_command      = STC_GET_WIFI_SSID;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_get_wifi_passkey()
{
  handled_command      = STC_GET_WIFI_PASSKEY;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_test_flash()
{
  handled_command      = STC_TEST_FLASH;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_get_rfid_content()
{
  handled_command      = STC_GET_RFID_CONTENT;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SET_RFID_CONTENT;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_get_version()
{
  handled_command      = STC_GET_VERSION;
  handled_command_kind = STCK_REQUEST;
}
void strato_test_interface_request_on_get_mac()
{
  handled_command      = STC_GET_MAC;
  handled_command_kind = STCK_REQUEST;
}

void strato_test_interface_reply_on_unknown(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  handled_command      = STC_UNKNOWN;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_ping()
{
  handled_command      = STC_PING;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_relay(const strato_test_interface_set_relay_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_RELAY;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_AI_MODE;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_ai(const strato_test_interface_read_ai_value_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_AI;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_AO_MODE;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_AO_VALUE;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_radar(const strato_test_interface_read_radar_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_RADAR;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_co2(const strato_test_interface_read_co2_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_CO2;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_temp(const strato_test_interface_read_temp_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_TEMP;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_hum(const strato_test_interface_read_hum_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_HUM;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_LCD_TEST;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_lum(const strato_test_interface_read_lum_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_LUM;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_read_buttons(const strato_test_interface_read_buttons_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_READ_BUTTONS;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_receive_rs485(const strato_test_interface_receive_rs485_reply_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_RECEIVE_RS485;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_send_rs485(const strato_test_interface_send_rs485_reply_payload_t *request_payload)
{
  UNUSED(request_payload);
  handled_command      = STC_SEND_RS485;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_get_wifi_ssid(const strato_test_interface_get_wifi_ssid_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_GET_WIFI_SSID;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_get_wifi_passkey(const strato_test_interface_get_wifi_passkey_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_GET_WIFI_PASSKEY;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_test_flash(const strato_test_interface_test_flash_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_TEST_FLASH;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_get_rfid_content(const strato_test_interface_get_rfid_content_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_GET_RFID_CONTENT;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_SET_RFID_CONTENT;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_get_version(const strato_test_interface_get_version_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_GET_VERSION;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_get_mac(const strato_test_interface_get_mac_reply_payload_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_GET_MAC;
  handled_command_kind = STCK_REPLY;
}
void strato_test_interface_reply_on_error(const strato_test_interface_error_t *reply_payload)
{
  UNUSED(reply_payload);
  handled_command      = STC_ERROR;
  handled_command_kind = STCK_REPLY;
}

void reset_global()
{
  handled_command      = STC_UNKNOWN;
  handled_command_kind = STCK_UNKNOWN;
}

void check_global(strato_test_interface_command_e command, bool is_request)
{
  EXPECT_EQ(command, handled_command);
  EXPECT_EQ(is_request ? STCK_REQUEST : STCK_REPLY, handled_command_kind);
}

strato_test_interface_conversion_result_e serialize_command(strato_test_interface_command_e command, bool is_request, const void *payload, unsigned char *shared_buffer, unsigned short *shared_buffer_length)
{
  *shared_buffer_length = 256;
  return strato_test_interface_serialize_command(command, is_request ? 1 : 0, payload, shared_buffer, shared_buffer_length);
}

template <typename T>
void check_command(strato_test_interface_command_e command, bool is_request, const T *payload, std::function<void(void *)> check_members)
{
  reset_global();
  //  Check serialization-deserialization
  T              recv_obj             = {};
  unsigned char  shared_buffer[256]   = {};
  unsigned short shared_buffer_length = 256;
  auto           serialization_result = serialize_command(command, is_request, payload, shared_buffer, &shared_buffer_length);
  EXPECT_EQ(serialization_result, STCR_SUCCESS);
  EXPECT_EQ(shared_buffer[0], command);
  EXPECT_EQ(shared_buffer[1], is_request ? 1 : 0);
  auto deserialization_result = strato_test_interface_deserialize_command(shared_buffer, shared_buffer_length, command, is_request ? 1 : 0, &recv_obj);
  EXPECT_EQ(deserialization_result, STCR_SUCCESS);
  check_members(&recv_obj);

  //  Check dispatching
  int *user = new int;
  strato_test_interface_dispatcher(shared_buffer, shared_buffer_length);
  check_global(command, is_request);
  delete user;
}

void check_command(strato_test_interface_command_e command, bool is_request)
{
  reset_global();
  //  Check serialization-deserialization
  unsigned char  shared_buffer[256]   = {};
  unsigned short shared_buffer_length = 256;
  auto           serialization_result = serialize_command(command, is_request, nullptr, shared_buffer, &shared_buffer_length);
  EXPECT_EQ(serialization_result, STCR_SUCCESS);
  EXPECT_EQ(shared_buffer[0], command);
  EXPECT_EQ(shared_buffer[1], is_request ? 1 : 0);
  auto conversion_result = strato_test_interface_deserialize_command(shared_buffer, shared_buffer_length, command, is_request ? 1 : 0, nullptr);
  EXPECT_EQ(conversion_result, STCR_SUCCESS);

  //  Check dispatching
  strato_test_interface_dispatcher(shared_buffer, shared_buffer_length);
  check_global(command, is_request);
}

TEST(test, ping)
{
  check_command(STC_PING, false);
  check_command(STC_PING, true);
}

TEST(test, set_relay)
{
  strato_test_interface_set_relay_payload_t payload       = { .index = 12, .state = 23 };
  auto                                      check_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_set_relay_payload_t *>(o);
    EXPECT_EQ(obj->index, 12);
    EXPECT_EQ(obj->state, 23);
  };
  check_command(STC_SET_RELAY, false, &payload, check_members);
  check_command(STC_SET_RELAY, true, &payload, check_members);
}

TEST(test, set_ai_mode)
{
  strato_test_interface_set_ai_mode_payload_t payload = {
    .index = 0x9E,
    .mode  = strato_test_interface_set_ai_mode_payload_t::STAIM_THERM,
  };
  auto check_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_set_ai_mode_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x9E);
    EXPECT_EQ(obj->mode, strato_test_interface_set_ai_mode_payload_t::STAIM_THERM);
  };
  check_command(STC_SET_AI_MODE, false, &payload, check_members);
  check_command(STC_SET_AI_MODE, true, &payload, check_members);
}

TEST(test, read_ai)
{
  strato_test_interface_read_ai_value_request_payload_t request_payload       = { .index = 0x34 };
  auto                                                  check_request_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_ai_value_request_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x34);
  };
  check_command(STC_READ_AI, true, &request_payload, check_request_members);

  strato_test_interface_read_ai_value_reply_payload_t reply_payload       = { .index = 0x5D, .value = 0x1122 };
  auto                                                check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_ai_value_reply_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x5D);
    EXPECT_EQ(obj->value, 0x1122);
  };
  check_command(STC_READ_AI, false, &reply_payload, check_reply_members);
}

TEST(test, set_ao_mode)
{
  strato_test_interface_set_ao_mode_payload_t payload       = { .mode = strato_test_interface_set_ao_mode_payload_t::STAOM_AOUT };
  auto                                        check_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_set_ao_mode_payload_t *>(o);
    EXPECT_EQ(obj->mode, strato_test_interface_set_ao_mode_payload_t::STAOM_AOUT);
  };
  check_command(STC_SET_AO_MODE, true, &payload, check_members);
  check_command(STC_SET_AO_MODE, false, &payload, check_members);
}

TEST(test, set_ao_value)
{
  strato_test_interface_set_ao_value_payload_t payload       = { .index = 0x12, .value = 0x64 };
  auto                                         check_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_set_ao_value_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x12);
    EXPECT_EQ(obj->value, 0x64);
  };
  check_command(STC_SET_AO_VALUE, true, &payload, check_members);
  check_command(STC_SET_AO_VALUE, false, &payload, check_members);
}

TEST(test, read_radar)
{
  strato_test_interface_read_radar_reply_payload_t reply_payload       = { .value = 0x15 };
  auto                                             check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_radar_reply_payload_t *>(o);
    EXPECT_EQ(obj->value, 0x15);
  };
  check_command(STC_READ_RADAR, true);
  check_command(STC_READ_RADAR, false, &reply_payload, check_reply_members);
}

TEST(test, read_co2)
{
  strato_test_interface_read_co2_reply_payload_t reply_payload       = { .value = 0xF55F };
  auto                                           check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_co2_reply_payload_t *>(o);
    EXPECT_EQ(obj->value, 0xF55F);
  };
  check_command(STC_READ_CO2, true);
  check_command(STC_READ_CO2, false, &reply_payload, check_reply_members);
}

TEST(test, read_temp)
{
  strato_test_interface_read_temp_reply_payload_t reply_payload = {
    .channel = STRTC_MIDDLE,
    .value   = 3.141592,
  };
  auto check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_temp_reply_payload_t *>(o);
    EXPECT_EQ(obj->channel, STRTC_MIDDLE);
    EXPECT_NEAR(obj->value, 3.141592, 0.01);
  };
  check_command(STC_READ_TEMP, true);
  check_command(STC_READ_TEMP, false, &reply_payload, check_reply_members);
}

TEST(test, read_hum)
{
  strato_test_interface_read_hum_reply_payload_t reply_payload       = { .value = 0x80AA };
  auto                                           check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_hum_reply_payload_t *>(o);
    EXPECT_EQ(obj->value, 0x80AA);
  };
  check_command(STC_READ_HUM, true);
  check_command(STC_READ_HUM, false, &reply_payload, check_reply_members);
}

TEST(test, set_lcd_test)
{
  strato_test_interface_set_lcd_test_payload_t payload = {
    .screen                   = strato_test_interface_set_lcd_test_payload_t::STSLT_TEST,
    .backlight_brightness_pct = 0x5D,
  };
  auto check_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_set_lcd_test_payload_t *>(o);
    EXPECT_EQ(obj->screen, strato_test_interface_set_lcd_test_payload_t::STSLT_TEST);
    EXPECT_EQ(obj->backlight_brightness_pct, 0x5D);
  };
  check_command(STC_SET_LCD_TEST, true, &payload, check_members);
  check_command(STC_SET_LCD_TEST, false, &payload, check_members);
}

TEST(test, read_lum)
{
  strato_test_interface_read_lum_reply_payload_t reply_payload       = { .value = 0x37 };
  auto                                           check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_lum_reply_payload_t *>(o);
    EXPECT_EQ(obj->value, 0x37);
  };
  check_command(STC_READ_LUM, true);
  check_command(STC_READ_LUM, false, &reply_payload, check_reply_members);
}

TEST(test, read_button)
{
  strato_test_interface_read_buttons_reply_payload_t reply_payload = {
    .pressed = { 0xFA, 0x23, 0x52, 0xEB }
  };
  auto check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_read_buttons_reply_payload_t *>(o);
    EXPECT_EQ(obj->pressed[0], 0xFA);
    EXPECT_EQ(obj->pressed[1], 0x23);
    EXPECT_EQ(obj->pressed[2], 0x52);
    EXPECT_EQ(obj->pressed[3], 0xEB);
  };
  check_command(STC_READ_BUTTONS, true);
  check_command(STC_READ_BUTTONS, false, &reply_payload, check_reply_members);
}

TEST(test, receive_rs485)
{
  strato_test_interface_receive_rs485_request_payload_t request_payload       = { .index = 0x55, .length = 0xAA };
  auto                                                  check_request_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_send_rs485_request_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x55);
    EXPECT_EQ(obj->length, 0xAA);
  };
  strato_test_interface_receive_rs485_reply_payload_t reply_payload       = { .index = 0x41, .result = strato_test_interface_receive_rs485_reply_payload_t::STRX_RS485_SUCCESS };
  auto                                                check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_receive_rs485_reply_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x41);
    EXPECT_EQ(obj->result, strato_test_interface_receive_rs485_reply_payload_t::STRX_RS485_SUCCESS);
  };
  check_command(STC_RECEIVE_RS485, true, &request_payload, check_request_members);
  check_command(STC_RECEIVE_RS485, false, &reply_payload, check_reply_members);
}

TEST(test, send_rs485)
{
  strato_test_interface_send_rs485_request_payload_t request_payload       = { .index = 0x55, .length = 0xAA, .delay_ms = 0xCAFEFADE };
  auto                                               check_request_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_send_rs485_request_payload_t *>(o);
    EXPECT_EQ(obj->index, 0x55);
    EXPECT_EQ(obj->length, 0xAA);
    EXPECT_EQ(obj->delay_ms, 0xCAFEFADE);
  };
  strato_test_interface_send_rs485_reply_payload_t reply_payload       = { .index = 0xE1, .result = strato_test_interface_send_rs485_reply_payload_t::STTX_RS485_ERROR_NO_DATA };
  auto                                             check_reply_members = [](void *o) {
    auto *obj = static_cast<strato_test_interface_send_rs485_reply_payload_t *>(o);
    EXPECT_EQ(obj->index, 0xE1);
    EXPECT_EQ(obj->result, strato_test_interface_send_rs485_reply_payload_t::STTX_RS485_ERROR_NO_DATA);
  };
  check_command(STC_SEND_RS485, true, &request_payload, check_request_members);
  check_command(STC_SEND_RS485, false, &reply_payload, check_reply_members);
}

TEST(test, get_wifi_ssid)
{
  strato_test_interface_get_wifi_ssid_reply_payload_t reply_payload = {
    .length = 6,
    .ssid   = "coucou",
  };
  auto check_reply_members = [&reply_payload](void *o) {
    auto *obj = static_cast<strato_test_interface_get_wifi_ssid_reply_payload_t *>(o);
    EXPECT_EQ(obj->length, 6);
    EXPECT_NE(obj->ssid, nullptr);
    for (int i = 0; i < obj->length; ++i) { EXPECT_EQ(reply_payload.ssid[i], obj->ssid[i]); }
  };
  check_command(STC_GET_WIFI_SSID, true);
  check_command(STC_GET_WIFI_SSID, false, &reply_payload, check_reply_members);
}

TEST(test, get_wifi_passkey)
{
  strato_test_interface_get_wifi_passkey_reply_payload_t reply_payload = {
    .length  = 6,
    .passkey = "coucou",
  };
  auto check_reply_members = [&reply_payload](void *o) {
    auto *obj = static_cast<strato_test_interface_get_wifi_passkey_reply_payload_t *>(o);
    EXPECT_EQ(obj->length, 6);
    EXPECT_NE(obj->passkey, nullptr);
    for (int i = 0; i < obj->length; ++i) { EXPECT_EQ(reply_payload.passkey[i], obj->passkey[i]); }
  };
  check_command(STC_GET_WIFI_PASSKEY, true);
  check_command(STC_GET_WIFI_PASSKEY, false, &reply_payload, check_reply_members);
}

TEST(test, test_flash)
{
  strato_test_interface_test_flash_reply_payload_t reply_payload       = { .result = 0x7e };
  auto                                             check_reply_members = [&reply_payload](void *o) {
    auto *obj = static_cast<strato_test_interface_test_flash_reply_payload_t *>(o);
    EXPECT_EQ(reply_payload.result, obj->result);
  };
  check_command(STC_TEST_FLASH, true);
  check_command(STC_TEST_FLASH, false, &reply_payload, check_reply_members);
}

TEST(test, get_rfid_content)
{
  unsigned char                                          reply_content_buffer[] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf };
  strato_test_interface_get_rfid_content_reply_payload_t reply_payload          = { .length = 16, .content = reply_content_buffer };
  auto                                                   check_reply_members    = [&reply_payload](void *o) {
    auto *obj = static_cast<strato_test_interface_get_rfid_content_reply_payload_t *>(o);
    EXPECT_EQ(obj->length, reply_payload.length);
    EXPECT_NE(obj->content, nullptr);
    for (int i = 0; i < obj->length; ++i) { EXPECT_EQ(obj->content[i], reply_payload.content[i]); }
  };
  check_command(STC_GET_RFID_CONTENT, true);
  check_command(STC_GET_RFID_CONTENT, false, &reply_payload, check_reply_members);
}

TEST(test, set_rfid_content)
{
  unsigned char                                    content_buffer[] = { 0xf, 0xe, 0xd, 0xc, 0xb, 0xa, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
  strato_test_interface_set_rfid_content_payload_t payload          = { .length = 16, .content = content_buffer };
  auto                                             check_members    = [&payload](void *o) {
    auto *obj = static_cast<strato_test_interface_set_rfid_content_payload_t *>(o);
    EXPECT_EQ(obj->length, payload.length);
    EXPECT_NE(obj->content, nullptr);
    for (int i = 0; i < obj->length; ++i) { EXPECT_EQ(obj->content[i], payload.content[i]); };
  };
  check_command(STC_SET_RFID_CONTENT, true, &payload, check_members);
  check_command(STC_SET_RFID_CONTENT, false, &payload, check_members);
}

TEST(test, get_version)
{
  strato_test_interface_get_version_reply_payload_t payload = {};
  memcpy(payload.firmwareVersion, "0.123", 5);
  memcpy(payload.bootloaderVersion, "4.567", 5);
  memcpy(payload.hardwareVersion, "890", 3);
  memcpy(payload.applicationVersion, "3.210", 5);
  auto check_members = [&payload](void *o) {
    auto       *obj          = static_cast<strato_test_interface_get_version_reply_payload_t *>(o);
    static auto compareArray = [](const char *a1, const char *a2, std::size_t len) {
      for (auto i = 0; i < len; ++i) { EXPECT_EQ(a1[i], a2[i]); }
    };
    compareArray(payload.firmwareVersion, obj->firmwareVersion, 5);
    compareArray(payload.bootloaderVersion, obj->bootloaderVersion, 5);
    compareArray(payload.hardwareVersion, obj->hardwareVersion, 3);
    compareArray(payload.applicationVersion, obj->applicationVersion, 5);
  };
  check_command(STC_GET_VERSION, true);
  check_command(STC_GET_VERSION, false, &payload, check_members);
}

TEST(test, get_mac)
{
  strato_test_interface_get_mac_reply_payload_t reply_payload = {
    .mac = { 0x12, 0x34, 0x45, 0x67, 0x89, 0xAB }
  };
  auto check_reply_members = [&reply_payload](void *o) {
    auto *obj = static_cast<strato_test_interface_get_mac_reply_payload_t *>(o);
    for (int i = 0; i < 6; ++i) { EXPECT_EQ(reply_payload.mac[i], obj->mac[i]); }
  };
  check_command(STC_GET_MAC, true);
  check_command(STC_GET_MAC, false, &reply_payload, check_reply_members);
}

TEST(test, error)
{
  strato_test_interface_error_t payload       = { .command = STC_PING };
  auto                          check_members = [&payload](void *o) {
    auto *obj = static_cast<strato_test_interface_error_t *>(o);
    EXPECT_EQ(obj->command, payload.command);
  };
  check_command(STC_ERROR, false, &payload, check_members);
}
