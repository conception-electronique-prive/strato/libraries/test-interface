#include "strato_test_interface.h"

#include "deserializer.h"
#include "serializer.h"
#include "utils.h"

#ifdef __GNUC__
#define st_weak __attribute__((weak))
#endif

#undef UNUSED
#define UNUSED(x) (void)x

#define REQUEST 1
#define REPLY   0

strato_test_interface_conversion_result_e strato_test_interface_serialize_command(strato_test_interface_command_e command, unsigned char is_request, const void *payload, unsigned char *buffer, unsigned short *length)
{
  stc_conversion_result_e (*serialize_request_handlers[STC_MAX])(const void *, unsigned char *, unsigned short *) = {
    stc_serializer_handle_no_payload,            /* UNKNOWN */
    stc_serializer_handle_no_payload,            /* PING */
    stc_serialize_set_relay_payload,             /* SET RELAY */
    stc_serialize_set_ai_mode_payload,           /* SET AI MODE */
    stc_serialize_read_ai_value_request_payload, /* READ AI */
    stc_serialize_set_ao_mode_payload,           /* SET AO MODE */
    stc_serialize_set_ao_value_payload,          /* SET AO VALUE */
    stc_serializer_handle_no_payload,            /* READ RADAR */
    stc_serializer_handle_no_payload,            /* READ CO2 */
    stc_serializer_handle_no_payload,            /* READ TEMP */
    stc_serializer_handle_no_payload,            /* READ HUM */
    stc_serialize_set_lcd_test_payload,          /* SET LCD TEST */
    stc_serializer_handle_no_payload,            /* READ LUM */
    stc_serializer_handle_no_payload,            /* READ BUTTONS */
    stc_serialize_receive_rs485_request_payload, /* RECEIVE RS485 */
    stc_serialize_send_rs485_request_payload,    /* SEND RS485 */
    stc_serializer_handle_no_payload,            /* GET WIFI SSID */
    stc_serializer_handle_no_payload,            /* GET WIFI PASSKEY */
    stc_serializer_handle_no_payload,            /* TEST FLASH */
    stc_serializer_handle_no_payload,            /* GET RFID CONTENT */
    stc_serialize_set_rfid_content_payload,      /* SET RFID CONTENT */
    stc_serializer_handle_no_payload,            /* GET VERSION */
    stc_serializer_handle_no_payload,            /* GET MAC */
    stc_serialize_error_payload                  /* ERROR */
  };
  stc_conversion_result_e (*serialize_reply_handlers[STC_MAX])(const void *, unsigned char *, unsigned short *) = {
    stc_serializer_handle_no_payload,             /* UNKNOWN */
    stc_serializer_handle_no_payload,             /* PING */
    stc_serialize_set_relay_payload,              /* SET RELAY */
    stc_serialize_set_ai_mode_payload,            /* SET AI MODE */
    stc_serialize_read_ai_value_reply_payload,    /* READ AI */
    stc_serialize_set_ao_mode_payload,            /* SET AO MODE */
    stc_serialize_set_ao_value_payload,           /* SET AO VALUE */
    stc_serialize_read_radar_reply_payload,       /* READ RADAR */
    stc_serialize_read_co2_reply_payload,         /* READ CO2 */
    stc_serialize_read_temp_reply_payload,        /* READ TEMP */
    stc_serialize_read_hum_reply_payload,         /* READ HUM */
    stc_serialize_set_lcd_test_payload,           /* SET LCD TEST */
    stc_serialize_read_lum_reply_payload,         /* READ LUM */
    stc_serialize_read_buttons_reply_payload,     /* READ BUTTONS */
    stc_serialize_receive_rs485_reply_payload,    /* RECEIVE RS485 */
    stc_serialize_send_rs485_reply_payload,       /* SEND RS485 */
    stc_serialize_get_wifi_ssid_reply_payload,    /* GET WIFI SSID */
    stc_serialize_get_wifi_passkey_reply_payload, /* GET WIFI PASSKEY */
    stc_serialize_test_flash_reply_payload,       /* TEST FLASH */
    stc_serialize_get_rfid_content_reply_payload, /* GET RFID CONTENT */
    stc_serialize_set_rfid_content_payload,       /* SET RFID CONTENT */
    stc_serialize_get_version_reply_payload,      /* GET VERSION */
    stc_serialize_get_mac_reply_payload,          /* GET MAC */
    stc_serialize_error_payload                   /* ERROR */
  };
  stc_conversion_result_e result;
  if (STC_MAX <= command) {
    *length = 0;
    return STCR_ERROR;
  }
  if (*length < 2) {
    *length = 0;
    return STCR_ERROR;
  }
  stc_uchar_to_buffer(command, buffer);
  stc_uchar_to_buffer(is_request, buffer + 1);
  buffer  = buffer + 2;
  *length = *length - 2;

  result  = is_request != 0 ? serialize_request_handlers[command](payload, buffer, length) : serialize_reply_handlers[command](payload, buffer, length);
  *length = result == STC_CONVERSION_SUCCESS ? *length + 2 : 0;
  return STCR_SUCCESS;
}

strato_test_interface_conversion_result_e strato_test_interface_deserialize_command(const unsigned char *buffer, unsigned short length, strato_test_interface_command_e command, unsigned char is_request, void *payload)
{
  stc_conversion_result_e (*deserialize_request_handlers[STC_MAX])(const unsigned char *, unsigned short, void *) = {
    stc_parse_handle_no_payload,             /* UNKNOWN */
    stc_parse_handle_no_payload,             /* PING */
    stc_parse_set_relay_payload,             /* SET RELAY */
    stc_parse_set_ai_mode_payload,           /* SET AI MODE */
    stc_parse_read_ai_value_request_payload, /* READ AI */
    stc_parse_set_ao_mode_payload,           /* SET AO MODE */
    stc_parse_set_ao_value_payload,          /* SET AO VALUE */
    stc_parse_handle_no_payload,             /* READ RADAR */
    stc_parse_handle_no_payload,             /* READ CO2 */
    stc_parse_handle_no_payload,             /* READ TEMP */
    stc_parse_handle_no_payload,             /* READ HUM */
    stc_parse_set_lcd_test_payload,          /* SET LCD TEST */
    stc_parse_handle_no_payload,             /* READ LUM */
    stc_parse_handle_no_payload,             /* READ BUTTONS */
    stc_parse_receive_rs485_request_payload, /* RECEIVE RS485 */
    stc_parse_send_rs485_request_payload,    /* SEND RS485 */
    stc_parse_handle_no_payload,             /* GET WIFI SSID */
    stc_parse_handle_no_payload,             /* GET WIFI PASSKEY */
    stc_parse_handle_no_payload,             /* TEST FLASH */
    stc_parse_handle_no_payload,             /* GET RFID CONTENT */
    stc_parse_set_rfid_content_payload,      /* SET RFID CONTENT */
    stc_parse_handle_no_payload,             /* GET VERSION */
    stc_parse_handle_no_payload,             /* GET MAC */
    stc_parse_error_payload                  /* ERROR */
  };
  stc_conversion_result_e (*deserialize_reply_handlers[STC_MAX])(const unsigned char *, unsigned short, void *) = {
    stc_parse_handle_no_payload,              /* UNKNOWN */
    stc_parse_handle_no_payload,              /* PING */
    stc_parse_set_relay_payload,              /* SET RELAY */
    stc_parse_set_ai_mode_payload,            /* SET AI MODE */
    stc_parse_read_ai_value_reply_payload,    /* READ AI */
    stc_parse_set_ao_mode_payload,            /* SET AO MODE */
    stc_parse_set_ao_value_payload,           /* SET AO VALUE */
    stc_parse_read_radar_reply_payload,       /* READ RADAR */
    stc_parse_read_co2_reply_payload,         /* READ CO2 */
    stc_parse_read_temp_reply_payload,        /* READ TEMP */
    stc_parse_read_hum_reply_payload,         /* READ HUM */
    stc_parse_set_lcd_test_payload,           /* SET LCD TEST */
    stc_parse_read_lum_reply_payload,         /* READ LUM */
    stc_parse_read_buttons_reply_payload,     /* READ BUTTONS */
    stc_parse_receive_rs485_reply_payload,    /* RECEIVE RS485 */
    stc_parse_send_rs485_reply_payload,       /* SEND RS485 */
    stc_parse_get_wifi_ssid_reply_payload,    /* GET WIFI SSID */
    stc_parse_get_wifi_passkey_reply_payload, /* GET WIFI PASSKEY */
    stc_parse_test_flash_reply_payload,       /* TEST FLASH */
    stc_parse_get_rfid_content_reply_payload, /* GET RFID CONTENT */
    stc_parse_set_rfid_content_payload,       /* SET RFID CONTENT */
    stc_parse_get_version_reply_payload,      /* GET VERSION */
    stc_parse_get_mac_reply_payload,          /* GET MAC */
    stc_parse_error_payload                   /* ERROR */
  };
  stc_conversion_result_e result;
  if (length < 2) { return STCR_ERROR; }
  if (stc_buffer_to_uchar(buffer) != command) { return STCR_ERROR; }
  if (stc_buffer_to_uchar(buffer + 1) != is_request) { return STCR_ERROR; }
  if (command < STC_UNKNOWN || STC_MAX <= command) { return STCR_ERROR; }

  length = length - 2;
  result = is_request != 0 ? deserialize_request_handlers[command](buffer + 2, length, payload) : deserialize_reply_handlers[command](buffer + 2, length, payload);
  if (result == STC_CONVERSION_FAILURE) { return STCR_ERROR; }
  return STCR_SUCCESS;
}

static void strato_test_interface_request_dispatcher_on_unknown(const unsigned char *buffer, unsigned short length) { strato_test_interface_request_on_unknown(buffer, length); }

static void strato_test_interface_request_dispatcher_on_ping(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_ping();
}

static void strato_test_interface_request_dispatcher_on_set_relay(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_relay_payload_t payload;
  strato_test_interface_conversion_result_e result = strato_test_interface_deserialize_command(buffer, length, STC_SET_RELAY, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_relay(&payload); }
}

static void strato_test_interface_request_dispatcher_on_set_ai_mode(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ai_mode_payload_t payload;
  strato_test_interface_conversion_result_e   result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AI_MODE, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_ai_mode(&payload); }
}

static void strato_test_interface_request_dispatcher_on_read_ai(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_ai_value_request_payload_t payload;
  strato_test_interface_conversion_result_e             result = strato_test_interface_deserialize_command(buffer, length, STC_READ_AI, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_read_ai(&payload); }
}

static void strato_test_interface_request_dispatcher_on_set_ao_mode(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ao_mode_payload_t payload;
  strato_test_interface_conversion_result_e   result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AO_MODE, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_ao_mode(&payload); }
}

static void strato_test_interface_request_dispatcher_on_set_ao_value(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ao_value_payload_t payload;
  strato_test_interface_conversion_result_e    result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AO_VALUE, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_ao_value(&payload); }
}

static void strato_test_interface_request_dispatcher_on_read_radar(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_read_radar();
}

static void strato_test_interface_request_dispatcher_on_read_co2(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_read_co2();
}

static void strato_test_interface_request_dispatcher_on_read_temp(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_temp_request_payload_t payload;
  strato_test_interface_conversion_result_e         result = strato_test_interface_deserialize_command(buffer, length, STC_READ_TEMP, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_read_temp(&payload); }
}

static void strato_test_interface_request_dispatcher_on_read_hum(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_read_hum();
}

static void strato_test_interface_request_dispatcher_on_set_lcd_test(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_lcd_test_payload_t payload;
  strato_test_interface_conversion_result_e    result = strato_test_interface_deserialize_command(buffer, length, STC_SET_LCD_TEST, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_lcd_test(&payload); }
}

static void strato_test_interface_request_dispatcher_on_read_lum(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_read_lum();
}

static void strato_test_interface_request_dispatcher_on_read_buttons(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_read_buttons();
}

static void strato_test_interface_request_dispatcher_on_receive_rs485(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_receive_rs485_request_payload_t payload;
  strato_test_interface_conversion_result_e             result = strato_test_interface_deserialize_command(buffer, length, STC_RECEIVE_RS485, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_receive_rs485(&payload); }
}

static void strato_test_interface_request_dispatcher_on_send_rs485(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_send_rs485_request_payload_t payload;
  strato_test_interface_conversion_result_e          result = strato_test_interface_deserialize_command(buffer, length, STC_SEND_RS485, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_send_rs485(&payload); }
}

static void strato_test_interface_request_dispatcher_on_get_wifi_ssid(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_get_wifi_ssid();
}

static void strato_test_interface_request_dispatcher_on_get_wifi_passkey(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_get_wifi_passkey();
}

static void strato_test_interface_request_dispatcher_on_test_flash(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_test_flash();
}

static void strato_test_interface_request_dispatcher_on_get_rfid_content(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_get_rfid_content();
}

static void strato_test_interface_request_dispatcher_on_set_rfid_content(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_rfid_content_payload_t payload;
  strato_test_interface_conversion_result_e        result = strato_test_interface_deserialize_command(buffer, length, STC_SET_RFID_CONTENT, REQUEST, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_request_on_set_rfid_content(&payload); }
}

static void strato_test_interface_request_dispatcher_on_get_version(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_get_version();
}

static void strato_test_interface_request_dispatcher_on_get_mac(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_request_on_get_mac();
}

static void strato_test_interface_reply_dispatcher_on_unknown(const unsigned char *buffer, unsigned short length) { strato_test_interface_reply_on_unknown(buffer, length); }
static void strato_test_interface_reply_dispatcher_on_ping(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
  strato_test_interface_reply_on_ping();
}
static void strato_test_interface_reply_dispatcher_on_set_relay(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_relay_payload_t payload;
  strato_test_interface_conversion_result_e result = strato_test_interface_deserialize_command(buffer, length, STC_SET_RELAY, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_relay(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_set_ai_mode(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ai_mode_payload_t payload;
  strato_test_interface_conversion_result_e   result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AI_MODE, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_ai_mode(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_ai(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_ai_value_reply_payload_t payload;
  strato_test_interface_conversion_result_e           result = strato_test_interface_deserialize_command(buffer, length, STC_READ_AI, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_ai(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_set_ao_mode(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ao_mode_payload_t payload;
  strato_test_interface_conversion_result_e   result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AO_MODE, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_ao_mode(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_set_ao_value(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_ao_value_payload_t payload;
  strato_test_interface_conversion_result_e    result = strato_test_interface_deserialize_command(buffer, length, STC_SET_AO_VALUE, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_ao_value(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_radar(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_radar_reply_payload_t payload;
  strato_test_interface_conversion_result_e        result = strato_test_interface_deserialize_command(buffer, length, STC_READ_RADAR, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_radar(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_co2(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_co2_reply_payload_t payload;
  strato_test_interface_conversion_result_e      result = strato_test_interface_deserialize_command(buffer, length, STC_READ_CO2, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_co2(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_temp(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_temp_reply_payload_t payload;
  strato_test_interface_conversion_result_e       result = strato_test_interface_deserialize_command(buffer, length, STC_READ_TEMP, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_temp(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_hum(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_hum_reply_payload_t payload;
  strato_test_interface_conversion_result_e      result = strato_test_interface_deserialize_command(buffer, length, STC_READ_HUM, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_hum(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_set_lcd_test(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_lcd_test_payload_t payload;
  strato_test_interface_conversion_result_e    result = strato_test_interface_deserialize_command(buffer, length, STC_SET_LCD_TEST, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_lcd_test(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_lum(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_lum_reply_payload_t payload;
  strato_test_interface_conversion_result_e      result = strato_test_interface_deserialize_command(buffer, length, STC_READ_LUM, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_lum(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_read_buttons(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_read_buttons_reply_payload_t payload;
  strato_test_interface_conversion_result_e          result = strato_test_interface_deserialize_command(buffer, length, STC_READ_BUTTONS, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_read_buttons(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_receive_rs485(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_receive_rs485_reply_payload_t payload;
  strato_test_interface_conversion_result_e           result = strato_test_interface_deserialize_command(buffer, length, STC_RECEIVE_RS485, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_receive_rs485(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_send_rs485(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_send_rs485_reply_payload_t payload;
  strato_test_interface_conversion_result_e        result = strato_test_interface_deserialize_command(buffer, length, STC_SEND_RS485, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_send_rs485(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_get_wifi_ssid(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_get_wifi_ssid_reply_payload_t payload;
  strato_test_interface_conversion_result_e           result = strato_test_interface_deserialize_command(buffer, length, STC_GET_WIFI_SSID, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_get_wifi_ssid(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_get_wifi_passkey(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_get_wifi_passkey_reply_payload_t payload;
  strato_test_interface_conversion_result_e              result = strato_test_interface_deserialize_command(buffer, length, STC_GET_WIFI_PASSKEY, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_get_wifi_passkey(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_test_flash(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_test_flash_reply_payload_t payload;
  strato_test_interface_conversion_result_e        result = strato_test_interface_deserialize_command(buffer, length, STC_TEST_FLASH, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_test_flash(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_get_rfid_content(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_get_rfid_content_reply_payload_t payload;
  strato_test_interface_conversion_result_e              result = strato_test_interface_deserialize_command(buffer, length, STC_GET_RFID_CONTENT, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_get_rfid_content(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_set_rfid_content(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_set_rfid_content_payload_t payload;
  strato_test_interface_conversion_result_e        result = strato_test_interface_deserialize_command(buffer, length, STC_SET_RFID_CONTENT, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_set_rfid_content(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_get_version(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_get_version_reply_payload_t payload;
  strato_test_interface_conversion_result_e         result = strato_test_interface_deserialize_command(buffer, length, STC_GET_VERSION, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_get_version(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_get_mac(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_get_mac_reply_payload_t payload;
  strato_test_interface_conversion_result_e     result = strato_test_interface_deserialize_command(buffer, length, STC_GET_MAC, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_get_mac(&payload); }
}
static void strato_test_interface_reply_dispatcher_on_error(const unsigned char *buffer, unsigned short length)
{
  strato_test_interface_error_t             payload;
  strato_test_interface_conversion_result_e result = strato_test_interface_deserialize_command(buffer, length, STC_ERROR, REPLY, &payload);
  if (result == STCR_SUCCESS) { strato_test_interface_reply_on_error(&payload); }
}

void strato_test_interface_dispatcher(const unsigned char *buffer, unsigned short length)
{
  void (*request_handlers[STC_MAX])(const unsigned char *, unsigned short) = {
    strato_test_interface_request_dispatcher_on_unknown,          /* UNKNOWN */
    strato_test_interface_request_dispatcher_on_ping,             /* PING */
    strato_test_interface_request_dispatcher_on_set_relay,        /* SET RELAY */
    strato_test_interface_request_dispatcher_on_set_ai_mode,      /* SET AI MODE */
    strato_test_interface_request_dispatcher_on_read_ai,          /* READ AI */
    strato_test_interface_request_dispatcher_on_set_ao_mode,      /* SET AO MODE */
    strato_test_interface_request_dispatcher_on_set_ao_value,     /* SET AO VALUE */
    strato_test_interface_request_dispatcher_on_read_radar,       /* READ RADAR */
    strato_test_interface_request_dispatcher_on_read_co2,         /* READ CO2 */
    strato_test_interface_request_dispatcher_on_read_temp,        /* READ TEMP */
    strato_test_interface_request_dispatcher_on_read_hum,         /* READ HUM */
    strato_test_interface_request_dispatcher_on_set_lcd_test,     /* SET LCD TEST */
    strato_test_interface_request_dispatcher_on_read_lum,         /* READ LUM */
    strato_test_interface_request_dispatcher_on_read_buttons,     /* READ BUTTONS */
    strato_test_interface_request_dispatcher_on_receive_rs485,    /* RECEIVE RS485 */
    strato_test_interface_request_dispatcher_on_send_rs485,       /* SEND RS485 */
    strato_test_interface_request_dispatcher_on_get_wifi_ssid,    /* GET WIFI SSID */
    strato_test_interface_request_dispatcher_on_get_wifi_passkey, /* GET WIFI PASSKEY */
    strato_test_interface_request_dispatcher_on_test_flash,       /* TEST FLASH */
    strato_test_interface_request_dispatcher_on_get_rfid_content, /* GET RFID CONTENT */
    strato_test_interface_request_dispatcher_on_set_rfid_content, /* SET RFID CONTENT */
    strato_test_interface_request_dispatcher_on_get_version,      /* GET VERSION */
    strato_test_interface_request_dispatcher_on_get_mac,          /* GET MAC */
    strato_test_interface_request_dispatcher_on_unknown,          /* ERROR */
  };

  void (*reply_handlers[STC_MAX])(const unsigned char *, unsigned short) = {
    strato_test_interface_reply_dispatcher_on_unknown,          /* UNKNOWN */
    strato_test_interface_reply_dispatcher_on_ping,             /* PING */
    strato_test_interface_reply_dispatcher_on_set_relay,        /* SET RELAY */
    strato_test_interface_reply_dispatcher_on_set_ai_mode,      /* SET AI MODE */
    strato_test_interface_reply_dispatcher_on_read_ai,          /* READ AI */
    strato_test_interface_reply_dispatcher_on_set_ao_mode,      /* SET AO MODE */
    strato_test_interface_reply_dispatcher_on_set_ao_value,     /* SET AO VALUE */
    strato_test_interface_reply_dispatcher_on_read_radar,       /* READ RADAR */
    strato_test_interface_reply_dispatcher_on_read_co2,         /* READ CO2 */
    strato_test_interface_reply_dispatcher_on_read_temp,        /* READ TEMP */
    strato_test_interface_reply_dispatcher_on_read_hum,         /* READ HUM */
    strato_test_interface_reply_dispatcher_on_set_lcd_test,     /* SET LCD TEST */
    strato_test_interface_reply_dispatcher_on_read_lum,         /* READ LUM */
    strato_test_interface_reply_dispatcher_on_read_buttons,     /* READ LUM */
    strato_test_interface_reply_dispatcher_on_receive_rs485,    /* RECEIVE RS485 */
    strato_test_interface_reply_dispatcher_on_send_rs485,       /* SEND RS485 */
    strato_test_interface_reply_dispatcher_on_get_wifi_ssid,    /* GET WIFI SSID */
    strato_test_interface_reply_dispatcher_on_get_wifi_passkey, /* GET WIFI PASSKEY */
    strato_test_interface_reply_dispatcher_on_test_flash,       /* TEST FLASH */
    strato_test_interface_reply_dispatcher_on_get_rfid_content, /* GET RFID CONTENT */
    strato_test_interface_reply_dispatcher_on_set_rfid_content, /* SET RFID CONTENT */
    strato_test_interface_reply_dispatcher_on_get_version,      /* GET VERSION */
    strato_test_interface_reply_dispatcher_on_get_mac,          /* GET MAC */
    strato_test_interface_reply_dispatcher_on_error,            /* ERROR */
  };
  unsigned char                   is_request = st_parse_is_request(buffer, length);
  strato_test_interface_command_e kind       = st_parse_command_kind(buffer, length);
  if (is_request == 0) { reply_handlers[kind](buffer, length); }
  else {
    request_handlers[kind](buffer, length);
  }
}

#ifdef __GNUC__
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnusedParameter"
st_weak void strato_test_interface_request_on_unknown(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
}

st_weak void strato_test_interface_request_on_ping(void) { }

st_weak void strato_test_interface_request_on_set_relay(const strato_test_interface_set_relay_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_read_ai(const strato_test_interface_read_ai_value_request_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_read_radar(void) { }

st_weak void strato_test_interface_request_on_read_co2(void) { }

st_weak void strato_test_interface_request_on_read_temp(const strato_test_interface_read_temp_request_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_read_hum(void) { }

st_weak void strato_test_interface_request_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_read_lum(void) { }

st_weak void strato_test_interface_request_on_read_buttons(void) { }

st_weak void strato_test_interface_request_on_receive_rs485(const strato_test_interface_receive_rs485_request_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_send_rs485(const strato_test_interface_send_rs485_request_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_get_wifi_ssid(void) { }

st_weak void strato_test_interface_request_on_get_wifi_passkey(void) { }

st_weak void strato_test_interface_request_on_test_flash(void) { }

st_weak void strato_test_interface_request_on_get_rfid_content(void) { }

st_weak void strato_test_interface_request_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t *request_payload) { UNUSED(request_payload); }

st_weak void strato_test_interface_request_on_get_version(void) { }

st_weak void strato_test_interface_request_on_get_mac(void) { }

st_weak void strato_test_interface_reply_on_unknown(const unsigned char *buffer, unsigned short length)
{
  UNUSED(buffer);
  UNUSED(length);
}

st_weak void strato_test_interface_reply_on_ping(void) { }

st_weak void strato_test_interface_reply_on_set_relay(const strato_test_interface_set_relay_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_set_ai_mode(const strato_test_interface_set_ai_mode_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_ai(const strato_test_interface_read_ai_value_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_set_ao_mode(const strato_test_interface_set_ao_mode_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_set_ao_value(const strato_test_interface_set_ao_value_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_radar(const strato_test_interface_read_radar_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_co2(const strato_test_interface_read_co2_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_temp(const strato_test_interface_read_temp_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_hum(const strato_test_interface_read_hum_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_set_lcd_test(const strato_test_interface_set_lcd_test_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_lum(const strato_test_interface_read_lum_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_read_buttons(const strato_test_interface_read_buttons_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_receive_rs485(const strato_test_interface_receive_rs485_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_send_rs485(const strato_test_interface_send_rs485_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_get_wifi_ssid(const strato_test_interface_get_wifi_ssid_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_get_wifi_passkey(const strato_test_interface_get_wifi_passkey_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_test_flash(const strato_test_interface_test_flash_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_get_rfid_content(const strato_test_interface_get_rfid_content_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_set_rfid_content(const strato_test_interface_set_rfid_content_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_get_version(const strato_test_interface_get_version_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_get_mac(const strato_test_interface_get_mac_reply_payload_t *reply_payload) { UNUSED(reply_payload); }

st_weak void strato_test_interface_reply_on_error(const strato_test_interface_error_t *reply_payload) { UNUSED(reply_payload); }
#pragma clang diagnostic pop
#endif
