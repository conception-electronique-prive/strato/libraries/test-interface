/**
 * @file    serializer.c
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "deserializer.h"

#include <string.h>

#include "strato_test_interface.h"

unsigned char  stc_buffer_to_uchar(const unsigned char *buffer) { return buffer[0]; }
unsigned short stc_buffer_to_ushort(const unsigned char *buffer) { return buffer[0] | buffer[1] << 8; }
unsigned long  stc_buffer_to_ulong(const unsigned char *buffer) { return buffer[0] | buffer[1] << 8 | buffer[2] << 16 | buffer[3] << 24; }
float          stc_buffer_to_float(const unsigned char *buffer)
{
  float          result = 0;
  unsigned char *ptr    = (unsigned char *)&result;
  ptr[0]                = buffer[0];
  ptr[1]                = buffer[1];
  ptr[2]                = buffer[2];
  ptr[3]                = buffer[3];
  return result;
}
stc_conversion_result_e stc_buffer_to_carray(const unsigned char *buffer, unsigned short buffer_length, unsigned char *value_buffer, unsigned short *value_length)
{
  unsigned short index = 0;
  if (buffer_length < *value_length) { return STC_CONVERSION_FAILURE; }
  *value_length = buffer_length;
  for (; index < buffer_length; ++buffer_length) { value_buffer[index] = buffer[index]; }
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_handle_no_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  (void)buffer;  /* silence */
  (void)payload; /* silence */
  return length == 0 ? STC_CONVERSION_SUCCESS : STC_CONVERSION_FAILURE;
}

stc_conversion_result_e stc_parse_set_relay_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_relay_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index = stc_buffer_to_uchar(buffer);
  args->state = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_set_ai_mode_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_ai_mode_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index = stc_buffer_to_uchar(buffer);
  args->mode  = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_ai_value_request_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_ai_value_request_payload_t *args = payload;
  if (length != 1) { return STC_CONVERSION_FAILURE; }
  args->index = stc_buffer_to_uchar(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_ai_value_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_ai_value_reply_payload_t *args = payload;
  if (length != 3) { return STC_CONVERSION_FAILURE; }
  args->index = stc_buffer_to_uchar(buffer);
  args->value = stc_buffer_to_ushort(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_set_ao_mode_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_ao_mode_payload_t *args = payload;
  if (length != 1) { return STC_CONVERSION_FAILURE; }
  args->mode = stc_buffer_to_uchar(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_set_ao_value_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_ao_value_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index = stc_buffer_to_uchar(buffer);
  args->value = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_radar_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_radar_reply_payload_t *args = payload;
  if (length != 1) { return STC_CONVERSION_FAILURE; }
  args->value = stc_buffer_to_uchar(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_co2_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_co2_reply_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->value = stc_buffer_to_ushort(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_temp_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_temp_reply_payload_t *args = payload;
  if (length != 5) { return STC_CONVERSION_FAILURE; }
  args->channel = stc_buffer_to_uchar(buffer);
  args->value   = stc_buffer_to_float(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_hum_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_hum_reply_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->value = stc_buffer_to_ushort(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_set_lcd_test_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_lcd_test_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->screen                   = stc_buffer_to_uchar(buffer);
  args->backlight_brightness_pct = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_lum_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_lum_reply_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->value = stc_buffer_to_ushort(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_read_buttons_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_read_buttons_reply_payload_t *args = payload;
  if (length != 4) { return STC_CONVERSION_FAILURE; }
  args->pressed[0] = stc_buffer_to_uchar(buffer);
  args->pressed[1] = stc_buffer_to_uchar(buffer + 1);
  args->pressed[2] = stc_buffer_to_uchar(buffer + 2);
  args->pressed[3] = stc_buffer_to_uchar(buffer + 3);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_receive_rs485_request_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_receive_rs485_request_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index  = stc_buffer_to_uchar(buffer);
  args->length = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_receive_rs485_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_receive_rs485_reply_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index  = stc_buffer_to_uchar(buffer);
  args->result = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_send_rs485_request_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_send_rs485_request_payload_t *args = payload;
  if (length != 6) { return STC_CONVERSION_FAILURE; }
  args->index    = stc_buffer_to_uchar(buffer);
  args->length   = stc_buffer_to_uchar(buffer + 1);
  args->delay_ms = stc_buffer_to_ulong(buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_send_rs485_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_send_rs485_reply_payload_t *args = payload;
  if (length != 2) { return STC_CONVERSION_FAILURE; }
  args->index  = stc_buffer_to_uchar(buffer);
  args->result = stc_buffer_to_uchar(buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_get_wifi_ssid_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_get_wifi_ssid_reply_payload_t *args = payload;
  if (length < 2) { return STC_CONVERSION_FAILURE; }
  args->length = stc_buffer_to_ushort(buffer);
  if (length != args->length + 2) { return STC_CONVERSION_FAILURE; }
  args->ssid = (const char *)buffer + 2;
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_get_wifi_passkey_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_get_wifi_passkey_reply_payload_t *args = payload;
  if (length < 2) { return STC_CONVERSION_FAILURE; }
  args->length = stc_buffer_to_ushort(buffer);
  if (length != args->length + 2) { return STC_CONVERSION_FAILURE; }
  args->passkey = (const char *)buffer + 2;
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_test_flash_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_test_flash_reply_payload_t *args = payload;
  if (length != 1) { return STC_CONVERSION_FAILURE; }
  args->result = stc_buffer_to_uchar(buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_get_rfid_content_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_get_rfid_content_reply_payload_t *args = payload;
  if (length < 2) { return STC_CONVERSION_FAILURE; }
  args->length = stc_buffer_to_ushort(buffer);
  if (length != args->length + 2) { return STC_CONVERSION_FAILURE; }
  args->content = buffer + 2;
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_set_rfid_content_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_set_rfid_content_payload_t *args = payload;
  if (length < 2) { return STC_CONVERSION_FAILURE; }
  args->length = stc_buffer_to_ushort(buffer);
  if (length != args->length + 2) { return STC_CONVERSION_FAILURE; }
  args->content = buffer + 2;
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_get_version_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_get_version_reply_payload_t *args = payload;
  if (length < 22) { return STC_CONVERSION_FAILURE; }
  memcpy(args->firmwareVersion, buffer, 6);
  memcpy(args->bootloaderVersion, buffer + 6, 6);
  memcpy(args->hardwareVersion, buffer + 12, 4);
  memcpy(args->applicationVersion, buffer + 16, 6);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_get_mac_reply_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_get_mac_reply_payload_t *args = payload;
  if (length != 6) { return STC_CONVERSION_FAILURE; }
  memcpy(args->mac, buffer, 6);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_parse_error_payload(const unsigned char *buffer, unsigned short length, void *payload)
{
  strato_test_interface_error_t *args = payload;
  if (length < 1) { return STC_CONVERSION_FAILURE; }
  args->command = stc_buffer_to_uchar(buffer);
  return STC_CONVERSION_SUCCESS;
}
