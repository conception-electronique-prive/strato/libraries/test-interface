/**
 * @file    serializer.h
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#ifndef STRATO_TEST_INTERFACE_SERIALIZER_H
#define STRATO_TEST_INTERFACE_SERIALIZER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "serializer.h"

unsigned char           stc_buffer_to_uchar(const unsigned char *buffer);
unsigned short          stc_buffer_to_ushort(const unsigned char *buffer);
unsigned long           stc_buffer_to_ulong(const unsigned char *buffer);
float                   stc_buffer_to_float(const unsigned char *buffer);
stc_conversion_result_e stc_buffer_to_carray(const unsigned char *buffer, unsigned short buffer_length, unsigned char *value_buffer, unsigned short *value_length);

stc_conversion_result_e stc_parse_handle_no_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_relay_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_ai_mode_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_ai_value_request_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_ai_value_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_ao_mode_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_ao_value_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_radar_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_co2_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_temp_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_hum_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_lcd_test_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_lum_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_read_buttons_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_receive_rs485_request_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_receive_rs485_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_send_rs485_request_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_send_rs485_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_get_wifi_ssid_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_get_wifi_passkey_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_test_flash_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_get_rfid_content_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_set_rfid_content_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_get_version_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_get_mac_reply_payload(const unsigned char *buffer, unsigned short length, void *payload);
stc_conversion_result_e stc_parse_error_payload(const unsigned char *buffer, unsigned short length, void *payload);

#ifdef __cplusplus
}
#endif

#endif
