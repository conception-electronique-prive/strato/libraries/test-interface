/**
 * @file    utils.c
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#include "utils.h"

strato_test_interface_command_e st_parse_command_kind(const unsigned char *buffer, unsigned short length)
{
  unsigned char kind;
  if (length < 1) { return STC_UNKNOWN; }
  /* We assume that length is greater or equal to 2 */
  kind = buffer[0];
  if (STC_MAX <= kind) { return STC_UNKNOWN; }
  return kind;
}

unsigned char st_parse_is_request(const unsigned char *buffer, unsigned short length)
{
  if (length < 2) { return 0; }
  /* We assume that length is greater or equal to 2 */
  return buffer[1] == 0 ? 0 : 1;
}
