/**
 * @file    deserializer.h
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */

#ifndef STRATO_TEST_INTERFACE_DESERIALIZER_H
#define STRATO_TEST_INTERFACE_DESERIALIZER_H

typedef enum {
  STC_CONVERSION_SUCCESS,
  STC_CONVERSION_FAILURE
} stc_conversion_result_e;

void stc_uchar_to_buffer(unsigned char value, unsigned char *buffer);
void stc_ushort_to_buffer(unsigned short value, unsigned char *buffer);
void stc_ulong_to_buffer(unsigned long value, unsigned char *buffer);
void stc_float_to_buffer(float value, unsigned char *buffer);
void stc_carray_to_buffer(const unsigned char *value, unsigned short length, unsigned char *buffer);

stc_conversion_result_e stc_serializer_handle_no_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_relay_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_ai_mode_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_ai_value_request_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_ai_value_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_ao_mode_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_ao_value_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_radar_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_co2_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_temp_request_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_temp_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_hum_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_lcd_test_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_lum_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_read_buttons_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_receive_rs485_request_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_receive_rs485_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_send_rs485_request_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_send_rs485_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_get_wifi_ssid_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_get_wifi_passkey_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_test_flash_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_get_rfid_content_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_set_rfid_content_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_get_version_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_get_mac_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length);
stc_conversion_result_e stc_serialize_error_payload(const void *payload, unsigned char *buffer, unsigned short *length);

#endif
