/**
 * @file    deserializer.c
 * @author  Paul Thomas
 * @date    5/27/2024
 * @brief
 *
 * @copyright
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version. This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <a
 * href=https://www.gnu.org/licenses/>https://www.gnu.org/licenses/</a>.
 */
#include "serializer.h"

#include <string.h>

#include "strato_test_interface.h"

void stc_uchar_to_buffer(unsigned char value, unsigned char *buffer) { buffer[0] = value; }
void stc_ushort_to_buffer(unsigned short value, unsigned char *buffer)
{
  unsigned char *ptr = (unsigned char *)&value;
  buffer[0]          = ptr[0];
  buffer[1]          = ptr[1];
}
void stc_ulong_to_buffer(unsigned long value, unsigned char *buffer)
{
  unsigned char *ptr = (unsigned char *)&value;
  buffer[0]          = ptr[0];
  buffer[1]          = ptr[1];
  buffer[2]          = ptr[2];
  buffer[3]          = ptr[3];
}
void stc_float_to_buffer(float value, unsigned char *buffer)
{
  unsigned char *ptr = (unsigned char *)&value;
  buffer[0]          = ptr[0];
  buffer[1]          = ptr[1];
  buffer[2]          = ptr[2];
  buffer[3]          = ptr[3];
}
void stc_carray_to_buffer(const unsigned char *value, unsigned short length, unsigned char *buffer)
{
  int index = 0;
  for (; index < length; ++index) { buffer[index] = value[index]; }
}

stc_conversion_result_e stc_serializer_handle_no_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  (void)payload; /* silence */
  (void)buffer;  /* silence */
  *length = 0;
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_relay_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_relay_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->state, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_ai_mode_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_ai_mode_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->mode, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_ai_value_request_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_ai_value_request_payload_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_uchar_to_buffer(args->index, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_ai_value_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_ai_value_reply_payload_t *args = payload;
  if (*length < 3) { return STC_CONVERSION_FAILURE; }
  *length = 3;
  stc_uchar_to_buffer(args->index, buffer);
  stc_ushort_to_buffer(args->value, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_ao_mode_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_ao_mode_payload_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_uchar_to_buffer(args->mode, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_ao_value_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_ao_value_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->value, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_radar_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_radar_reply_payload_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_ushort_to_buffer(args->value, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_co2_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_co2_reply_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_ushort_to_buffer(args->value, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_temp_request_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_temp_request_payload_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_uchar_to_buffer(args->channel, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_temp_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_temp_reply_payload_t *args = payload;
  if (*length < 5) { return STC_CONVERSION_FAILURE; }
  *length = 5;
  stc_uchar_to_buffer(args->channel, buffer);
  stc_float_to_buffer(args->value, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_hum_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_hum_reply_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_ushort_to_buffer(args->value, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_lcd_test_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_lcd_test_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->screen, buffer);
  stc_uchar_to_buffer(args->backlight_brightness_pct, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_lum_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_lum_reply_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_ushort_to_buffer(args->value, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_read_buttons_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_read_buttons_reply_payload_t *args = payload;
  if (*length < 4) { return STC_CONVERSION_FAILURE; }
  *length = 4;
  stc_uchar_to_buffer(args->pressed[0], buffer);
  stc_uchar_to_buffer(args->pressed[1], buffer + 1);
  stc_uchar_to_buffer(args->pressed[2], buffer + 2);
  stc_uchar_to_buffer(args->pressed[3], buffer + 3);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_receive_rs485_request_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_receive_rs485_request_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->length, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_receive_rs485_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_receive_rs485_reply_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->result, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_send_rs485_request_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_send_rs485_request_payload_t *args = payload;
  if (*length < 6) { return STC_CONVERSION_FAILURE; }
  *length = 6;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->length, buffer + 1);
  stc_ulong_to_buffer(args->delay_ms, buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_send_rs485_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_send_rs485_reply_payload_t *args = payload;
  if (*length < 2) { return STC_CONVERSION_FAILURE; }
  *length = 2;
  stc_uchar_to_buffer(args->index, buffer);
  stc_uchar_to_buffer(args->result, buffer + 1);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_get_wifi_ssid_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_get_wifi_ssid_reply_payload_t *args = payload;
  if (*length < 2 + args->length) { return STC_CONVERSION_FAILURE; }
  *length = args->length + 2;
  stc_ushort_to_buffer(args->length, buffer);
  stc_carray_to_buffer((const unsigned char *)args->ssid, args->length, buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_get_wifi_passkey_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_get_wifi_passkey_reply_payload_t *args = payload;
  if (*length < 2 + args->length) { return STC_CONVERSION_FAILURE; }
  *length = args->length + 2;
  stc_ushort_to_buffer(args->length, buffer);
  stc_carray_to_buffer((const unsigned char *)args->passkey, args->length, buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_test_flash_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_test_flash_reply_payload_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_uchar_to_buffer(args->result, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_get_rfid_content_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_get_rfid_content_reply_payload_t *args = payload;
  if (*length < 2 + args->length) { return STC_CONVERSION_FAILURE; }
  *length = args->length + 2;
  stc_uchar_to_buffer(args->length, buffer);
  stc_carray_to_buffer(args->content, args->length, buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_set_rfid_content_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_set_rfid_content_payload_t *args = payload;
  if (*length < 2 + args->length) { return STC_CONVERSION_FAILURE; }
  *length = args->length + 2;
  stc_uchar_to_buffer(args->length, buffer);
  stc_carray_to_buffer(args->content, args->length, buffer + 2);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_get_version_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_get_version_reply_payload_t *args = payload;
  if (*length < 22) { return STC_CONVERSION_FAILURE; }
  *length = 22;
  memcpy(buffer, args->firmwareVersion, 6);
  memcpy(buffer + 6, args->bootloaderVersion, 6);
  memcpy(buffer + 12, args->hardwareVersion, 4);
  memcpy(buffer + 16, args->applicationVersion, 6);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_get_mac_reply_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_get_mac_reply_payload_t *args = payload;
  if (*length < 6) { return STC_CONVERSION_FAILURE; }
  *length = 6;
  stc_carray_to_buffer(args->mac, 6, buffer);
  return STC_CONVERSION_SUCCESS;
}

stc_conversion_result_e stc_serialize_error_payload(const void *payload, unsigned char *buffer, unsigned short *length)
{
  const strato_test_interface_error_t *args = payload;
  if (*length < 1) { return STC_CONVERSION_FAILURE; }
  *length = 1;
  stc_uchar_to_buffer(args->command, buffer);
  return STC_CONVERSION_SUCCESS;
}
